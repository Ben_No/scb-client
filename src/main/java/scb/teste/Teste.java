package scb.teste;

public class Teste {
	public final String id;
    public final String senha;
    public final String email;

    public Teste(String id, String senha, String email) {
        this.id = id;
        this.senha = senha;
        this.email = email;
    }
}
