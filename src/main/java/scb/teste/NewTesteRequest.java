package scb.teste;

public class NewTesteRequest {
	public String id;
    public String senha;
    public String email;

    public NewTesteRequest() {
    }

    public NewTesteRequest(String senha, String email) {
        this.senha = senha;
        this.email = email;
    }
}
