package scb.teste;

/*import java.io.PrintWriter;
import java.io.StringWriter;*/

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import io.javalin.http.Context;

public class TesteController {
	
	private static final String REST_URI = "https://scb-server-teste.herokuapp.com";

    private static Client client = ClientBuilder.newClient();

    public static void aloMundoTeste(Context ctx) {		 
		String response =  client
					    .target(REST_URI)
					    .path("aloMundo")
					    .request(MediaType.APPLICATION_JSON)
					    .get(String.class);
		
		ctx.json(response);
    }
    
    public static void aloNomeTeste(Context ctx) {		 
		String nome = ctx.pathParam("nome");
    	
		String response =  client
					    .target(REST_URI)
					    .path("aloMundo/" + nome)
					    .request(MediaType.APPLICATION_JSON)
					    .get(String.class);
		
		ctx.json(response);
    }
}