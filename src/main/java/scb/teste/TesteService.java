package scb.teste;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
//import java.util.concurrent.atomic.AtomicInteger;

public class TesteService {

    private static Map<Integer, Teste> totens = new HashMap<>();
    //private static AtomicInteger lastId;

    static {
    	totens.put(0, new Teste("0", "1234", "daisdjiajsdisa"));
    	totens.put(1, new Teste("0", "1234", "daisdjiajsdisa"));
    	totens.put(2, new Teste("0", "1234", "daisdjiajsdisa"));
    	totens.put(3, new Teste("0", "1234", "daisdjiajsdisa"));
        //lastId = new AtomicInteger(totens.size());
    }

	/*
	 * public static void save(int numero) { int id = lastId.incrementAndGet();
	 * totens.put(id, new Teste(id, numero)); }
	 */

    public static Collection<Teste> getAll() {
        return totens.values();
    }

	/*
	 * public static void update(int totemId, int numero) { totens.put(totemId, new
	 * Teste(totemId, numero)); }
	 */

    public static Teste findById(int totemId) {
        return totens.get(totemId);
    }

    public static void delete(int totemId) {
    	totens.remove(totemId);
    }

}
